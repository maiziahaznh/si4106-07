package app.infiniverse.chamomile;


public interface AddorRemoveCallbacks {

    public void onAddProduct();
    public void onRemoveProduct();
}
